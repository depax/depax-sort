# Sorting

[![CircleCI](https://circleci.com/bb/depax/depax-sort.svg?style=svg)](https://circleci.com/bb/depax/depax-sort)
[![Todos](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-sort/latest/artifacts/0/shields/todos.svg)](#)
[![Features](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-sort/latest/artifacts/0/shields/features.svg)](#)
[![Coverage](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-sort/latest/artifacts/0/shields/features-coverage.svg)](#)
[![Documentation](https://img.shields.io/badge/documentation-🕮-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-sort/latest/artifacts/0/documentation/index.html)
[![Report](https://img.shields.io/badge/report-💣-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-sort/latest/artifacts/0/report)

## Installation

```js
import sort from "@depax/sort";

const data = [{
    value: "hello", weight: 10
}, {
    value: "world", weight: -10
}, {
    value: "foo"
}];

sort(data, "weight");
```

This will sort the array like;

```js
[{
    value: "world", weight: -10
}, {
    value: "foo"
}, {
    value: "hello", weight: 10
}]
```

This can also be reveresed like;

```js
sort(data, "weight", true);
```

and would return;

```js
[{
    value: "hello", weight: 10
}, {
    value: "foo"
}, {
    value: "world", weight: -10
}]
```

You can also use function's to return the key weight value, for example;

```js
[{
    value: "hello", weight: () => 10
}, {
    value: "world", weight: -10
}, {
    value: "foo"
}]
```

You can also sort object's like;

```js
const data = {
    world: "world",
    hello: { weight: -1 },
    foo: "bar",
    bar: { weight: 1 }
}

sort(data, "weight");
```

and this will return;

```js
{
    hello: { weight: -1 },
    world: "world",
    foo: "bar",
    bar: { weight: 1 }
}
```

### Array and Object

There is an attach function which will attach the sortByKey functions to the Array and Object classes. And this is
done by calling;

```js
import { attach } from "@depax/sort";
attach();
```

Then the sortByKey functions can be access like;

```js
// Array.
Array.sortByKey(data, "weight");
// - or -
data.sortByKey("weight");

// Object.
Object.sortByKey(data, "weight");
// - or -
data.sortByKey("weight");
```

### Clone and sort

There is also a method to clone and sort an Object or Array;

```js
import { CloneAndSort } from "@depax/sort";

// Sort by weight;
let result = CloneAndSort(data, "weight");

// Reverse sort by weight;
result = CloneAndSort(data, "weight", true);
```

The methods are also attachable to the Object and Array objects;

```js
let result = data.cloneAndSort("weight");

// Reverse.
result = data.cloneAndSort("weight", true);
```
