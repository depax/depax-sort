
/**
 * Provides the clone and sort method.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { IObject } from "./Lib/SortObject";
import Sort from "./Sort";

export default function CloneAndSort(data: any[] | IObject, key: string, reverse: boolean = false): any[] | IObject {
    const result = Object.assign(data instanceof Array ? [] : {}, data);
    Sort(result, key, reverse);

    return result;
}
