/**
 * Provides the sort method.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import SortArray from "./Lib/SortArray";
import SortObject, { IObject } from "./Lib/SortObject";

export default function Sort(data: any[] | IObject, key: string, reverse: boolean = false): void {
    if (data instanceof Array) {
        SortArray(data, key, reverse);
    } else {
        SortObject(data, key, reverse);
    }
}
