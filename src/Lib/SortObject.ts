/**
 * Provides the object sort function.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

export interface IObject {
    [name: string]: any;
}

/**
 * Sort an object by the provided key.
 *
 * @param data The object to sort.
 * @param key The key to use for sorting.
 * @param reverse Reverse the order.
 */
export default function SortObject(data: IObject, key: string, reverse: boolean): void {
    const keys = Object.keys(data);
    keys.sort((a, b) => {
        const aw = data[a][key] !== undefined ? (
            typeof data[a][key] === "function" ?
            parseInt(data[a][key](), 10) :
            parseInt(data[a][key], 10)
        ) : 0;
        const bw = data[b][key] !== undefined ? (
            typeof data[b][key] === "function" ?
            parseInt(data[b][key](), 10) :
            parseInt(data[b][key], 10)
        ) : 0;

        if (aw === bw) {
            return 0;
        }

        return (reverse) ?
            (aw > bw ? -1 : 1) :
            (aw < bw ? -1 : 1);
    });

    keys.forEach((field) => {
        const value = data[field];
        delete data[field];
        data[field] = value;
    });
}
