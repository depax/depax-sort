/**
 * Provides the array sort function.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/**
 * Sort an array by the provided key.
 *
 * @param data The array to sort.
 * @param key The key to use for sorting.
 * @param reverse Reverse the order.
 */
export default function SortArray(data: any[], key: string, reverse: boolean): void {
    data.sort((a, b) => {
        const aw = a[key] !== undefined ? (
            typeof a[key] === "function" ?
            parseInt(a[key](), 10) :
            parseInt(a[key], 10)
        ) : 0;
        const bw = b[key] !== undefined ? (
            typeof b[key] === "function" ?
            parseInt(b[key](), 10) :
            parseInt(b[key], 10)
        ) : 0;

        if (aw === bw) {
            return 0;
        }

        return (reverse) ?
            (aw > bw ? -1 : 1) :
            (aw < bw ? -1 : 1);
    });
}
