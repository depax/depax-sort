/**
 * Attach the sort methods to the Object and Array objects.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import CloneAndSort from "../CloneAndSort";
import SortArray from "./SortArray";
import SortObject from "./SortObject";

/**
 * Attach the sortByKey methods to the Array and Object classes.
 */
export default function Attach(): void {
    (Array as any).sortByKey = SortArray;
    /* istanbul ignore next */
    (Array.prototype as any).sortByKey = function(key: string, reverse: boolean = false) {
        (Array as any).sortByKey(this, key, reverse);
    };

    (Array as any).cloneAndSortByKey = CloneAndSort;
    /* istanbul ignore next */
    (Array.prototype as any).cloneAndSortByKey = function(key: string, reverse: boolean = false): any[] {
        return (Array as any).cloneAndSortByKey(this, key, reverse);
    };

    (Object as any).sortByKey = SortObject;
    /* istanbul ignore next */
    (Object.prototype as any).sortByKey = function(key: string, reverse: boolean = false) {
        (Object as any).sortByKey(this, key, reverse);
    };

    (Object as any).cloneAndSortByKey = CloneAndSort;
    /* istanbul ignore next */
    (Object.prototype as any).cloneAndSortByKey = function(key: string, reverse: boolean = false): object {
        return (Object as any).cloneAndSortByKey(this, key, reverse);
    };
}
