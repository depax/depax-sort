/**
 * Provides the step definitions for the feature tests.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import chai = require("chai");
import { Given, Then, When } from "cucumber";
import Sort, { CloneAndSort, IObject, Attach } from "../../src";

let convertedKey: string;
let data: any[] | IObject;
let clone: any[] | IObject;

function convertCallbacks(key: string, dataToConvert): void {
    if (dataToConvert instanceof Array) {
        dataToConvert.forEach((value, k) => {
            if (typeof value[key] === "string" && value[key][0] === "@") {
                const weight = parseInt(value[key].substr(1), 10);
                dataToConvert[k][key] = () => weight;
            }
        });
    } else {
        Object.keys(dataToConvert).forEach((k) => {
            const value = dataToConvert[k];
            if (typeof value[key] === "string" && value[key][0] === "@") {
                const weight = parseInt(value[key].substr(1), 10);
                dataToConvert[k][key] = () => weight;
            }
        });
    }
}

function convertCallbackValues(key: string, dataToConvert): void {
    if (dataToConvert instanceof Array) {
        dataToConvert.forEach((value, k) => {
            if (typeof value[key] === "function") {
                dataToConvert[k][key] = value[key]();
            }
        });
    } else {
        Object.keys(dataToConvert).forEach((k) => {
            const value = dataToConvert[k];
            if (typeof value[key] === "function") {
                dataToConvert[k][key] = value[key]();
            }
        });
    }
}

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/^I have an (object|array) like;$/, async (
    type: string,
    payload: string,
): Promise<void> => {
    const converted = JSON.parse(payload);
    if (
        (converted instanceof Array && type === "array") ||
        (converted instanceof Array === false && type === "object")
    ) {
        data = converted;
    } else {
        throw new Error("Data type mismatch.");
    }
});
//#endregion

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I convert keys "(\S*)" to callbacks$/, async (
    key: string,
): Promise<void> => {
    convertedKey = key;
    convertCallbacks(key, data);
});

When(/^I call the attach method$/, async(): Promise<void> => {
    Attach();
});

When(/^I sort the data by "(\S*)"$/, async (
    key: string,
): Promise<void> => {
    Sort(data, key);
});

When(/^I clone and sort the data by "(\S*)"$/, async (
    key: string,
): Promise<void> => {
    clone = CloneAndSort(data, key);
});

When(/^I reverse sort the data by "(\S*)"$/, async (
    key: string,
): Promise<void> => {
    Sort(data, key, true);
});

When(/^I clone and reverse sort the data by "(\S*)"$/, async (
    key: string,
): Promise<void> => {
    clone = CloneAndSort(data, key, true);
});
//#endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^The data should be;$/, async (
    payload: string,
): Promise<void> => {
    convertCallbackValues(convertedKey, data);
    chai.assert.deepEqual(data, JSON.parse(payload));
});

Then(/^The cloned data should be;$/, async (
    payload: string,
): Promise<void> => {
    convertCallbackValues(convertedKey, clone);
    chai.assert.deepEqual(clone, JSON.parse(payload));
});

Then(/^The (Array|Object) (class|reference) (does not have|has) the methods$/, async (
    type: string,
    ref: string,
    has: string,
): Promise<void> => {
    const assert = has !== "has" ? "isUndefined" : "isFunction";
    let actual;
    if (ref === "class") {
        actual = type === "Array" ? Array : Object;
    } else {
        actual = type === "Array" ? [] : {};
    }

    chai.assert[assert]((actual as any).sortByKey);
    chai.assert[assert]((actual as any).cloneAndSortByKey);
});
//#endregion
