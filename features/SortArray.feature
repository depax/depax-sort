@sort @array
Feature: Test array sorting

  Scenario: Should sort array by key
    Given I have an array like;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": 5
      }]
      """
     When I sort the data by "weight"
     Then The data should be;
      """
      [{
        "text": "bar", "weight": -11
      }, {
        "text": "hello"
      }, {
        "text": "world", "weight": 1
      }, {
        "text": "goodbye", "weight": 5
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }]
      """

  Scenario: Should reverse sort array by key
    Given I have an array like;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": 5
      }]
      """
     When I reverse sort the data by "weight"
     Then The data should be;
      """
      [{
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "goodbye", "weight": 5
      }, {
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "bar", "weight": -11
      }]
      """

  Scenario: Should clone and sort array by key
    Given I have an array like;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": 5
      }]
      """
     When I clone and sort the data by "weight"
     Then The data should be;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": 5
      }]
      """
      And The cloned data should be;
      """
      [{
        "text": "bar", "weight": -11
      }, {
        "text": "hello"
      }, {
        "text": "world", "weight": 1
      }, {
        "text": "goodbye", "weight": 5
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }]
      """

  Scenario: Should reverse sort array by key
    Given I have an array like;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": 5
      }]
      """
     When I clone and reverse sort the data by "weight"
     Then The data should be;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": 5
      }]
      """
      And The cloned data should be;
      """
      [{
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "goodbye", "weight": 5
      }, {
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "bar", "weight": -11
      }]
      """

  Scenario: Should sort array by key function
    Given I have an array like;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": "@10"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": "@5"
      }]
      """
     When I convert keys "weight" to callbacks
      And I sort the data by "weight"
     Then The data should be;
      """
      [{
        "text": "bar", "weight": -11
      }, {
        "text": "hello"
      }, {
        "text": "world", "weight": 1
      }, {
        "text": "goodbye", "weight": 5
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }]
      """

  Scenario: Should reverse sort array by key
    Given I have an array like;
      """
      [{
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "foo", "weight": "@10"
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "bar", "weight": -11
      }, {
        "text": "goodbye", "weight": "@5"
      }]
      """
     When I convert keys "weight" to callbacks
      And I reverse sort the data by "weight"
     Then The data should be;
      """
      [{
        "text": "foo", "weight": 10
      }, {
        "text": "foo", "weight": 10
      }, {
        "text": "goodbye", "weight": 5
      }, {
        "text": "world", "weight": 1
      }, {
        "text": "hello"
      }, {
        "text": "bar", "weight": -11
      }]
      """
