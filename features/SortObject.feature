@sort @object
Feature: Test object sorting

  Scenario: Should sort object by key
    Given I have an object like;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": 10 },
        "bar": { "weight": -11 },
        "goodbye": { "weight": 5 },
        "boom": { "weight": 10 }
      }
      """
     When I sort the data by "weight"
     Then The data should be;
      """
      {
        "bar": { "weight": -11 },
        "hello": "world",
        "world": { "weight": 1 },
        "goodbye": { "weight": 5 },
        "foo": { "weight": 10 },
        "boom": { "weight": 10 }
      }
      """

  Scenario: Should reverse sort object by key
    Given I have an object like;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": 10 },
        "bar": { "weight": -11 },
        "goodbye": { "weight": 5 },
        "boom": { "weight": 10 }
      }
      """
     When I reverse sort the data by "weight"
     Then The data should be;
      """
      {
        "foo": { "weight": 10 },
        "boom": { "weight": 10 },
        "goodbye": { "weight": 5 },
        "world": { "weight": 1 },
        "hello": "world",
        "bar": { "weight": -11 }
      }
      """

  Scenario: Should clone and sort object by key
    Given I have an object like;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": 10 },
        "bar": { "weight": -11 },
        "goodbye": { "weight": 5 }
      }
      """
     When I clone and sort the data by "weight"
     Then The data should be;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": 10 },
        "bar": { "weight": -11 },
        "goodbye": { "weight": 5 }
      }
      """
      And The cloned data should be;
      """
      {
        "bar": { "weight": -11 },
        "hello": "world",
        "world": { "weight": 1 },
        "goodbye": { "weight": 5 },
        "foo": { "weight": 10 }
      }
      """

  Scenario: Should reverse sort object by key
    Given I have an object like;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": 10 },
        "bar": { "weight": -11 },
        "goodbye": { "weight": 5 }
      }
      """
     When I clone and reverse sort the data by "weight"
     Then The data should be;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": 10 },
        "bar": { "weight": -11 },
        "goodbye": { "weight": 5 }
      }
      """
      And The cloned data should be;
      """
      {
        "foo": { "weight": 10 },
        "goodbye": { "weight": 5 },
        "world": { "weight": 1 },
        "hello": "world",
        "bar": { "weight": -11 }
      }
      """

  Scenario: Should sort object by key function
    Given I have an object like;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": "@10" },
        "bar": { "weight": -11 },
        "goodbye": { "weight": "@5" }
      }
      """
     When I convert keys "weight" to callbacks
      And I sort the data by "weight"
     Then The data should be;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": 10 },
        "bar": { "weight": -11 },
        "goodbye": { "weight": 5 }
      }
      """

  Scenario: Should reverse sort object by key
    Given I have an object like;
      """
      {
        "world": { "weight": 1 },
        "hello": "world",
        "foo": { "weight": "@10" },
        "bar": { "weight": -11 },
        "goodbye": { "weight": "@5" }
      }
      """
     When I convert keys "weight" to callbacks
      And I reverse sort the data by "weight"
     Then The data should be;
      """
      {
        "foo": { "weight": 10 },
        "goodbye": { "weight": 5 },
        "world": { "weight": 1 },
        "hello": "world",
        "bar": { "weight": -11 }
      }
      """
