@sort @attach
Feature: Test attached methods

  Scenario: Should have the attached methods on the classes
    Given The Array class does not have the methods
      And The Array reference does not have the methods
      And The Object class does not have the methods
      And The Object reference does not have the methods
     When I call the attach method
     Then The Array class has the methods
      And The Array reference has the methods
      And The Object class has the methods
      And The Object reference has the methods
